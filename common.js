(function() {
  var burger = document.querySelector('.burger');
  var nav = document.querySelector('#'+burger.dataset.target);
  burger.addEventListener('click', function(){
    burger.classList.toggle('is-active');
    nav.classList.toggle('is-active');
  });
})();

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.querySelector(".back-to-top").style.display = "inline-block";
  } else {
    document.querySelector(".back-to-top").style.display = "none";
  }
}

import Dictionary from './components/Dictionary.js';

let commonVm = new Vue({
  el: '#common-app',

  components: {
    'dictionary': Dictionary,
  },
  data: {
   showDict: false,
   version: '0.9.6',
 }
});