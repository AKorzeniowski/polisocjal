export default {
  name: 'dictionary',
  props: {
    prefix: {
      default: '',
      type: String
    }
  },
  data: () => (
   { 
     showDict: false,
     version: '0.9.7',
     prefix: '',
   }
  ),
  template: `
  <div>
	  <div v-if="showDict" class="modal is-active">
        <div class="modal-background"></div>
        <div class="modal-content content box">
          <h2>Słowniczek pojęć</h2>
          <dl>
            <dt>CKM</dt>
              <dd>Centrum Kształcenia Międzynarodowego</dd>
            <dt>Dochód utracony</dt>
              <dd>jest to utrata dochodu spowodowana utratą pracy, zasiłku, stypendium, emerytury bądź renty, a także wyrejestrowaniem lub zawieszeniem działalności gospodarczej w roku, którego dotyczą zaświadczenia z Urzędu Skarbowego lub po nim.</dd>
            <dt>Dochód uzyskany</dt>
              <dd>jest to dochód spowodowany uzyskaniem pracy, zasiłku, stypendium, emerytury bądź renty, a także rozpoczęciem lub wznowieniem działalności gospodarczej po roku, którego dotyczą zaświadczenia z Urzędu Skarbowego.</dd>
            <dt>DS</dt>
              <dd>dom studencki.</dd>
            <dt>Gospodarstwo domowe</dt>
              <dd>pod tym pojęciem kryje się najbliższa rodzina wnioskodawcy, czyli rodzice lub opiekunowie prawni lub faktyczni, utrzymujący studenta, a także ich pozostałe dzieci oraz współmałżonek i dzieci wnioskodawcy.</dd>
            <dt>Komisja stypendialna</dt>
              <dd>jest to organ uczelni przyznający świadczenia pomocy materialnej, w skład którego wchodzą zarówno studenci jak i pracownicy uczelni.</dd>
            <dt>Kwestura PŁ</dt>
              <dd>część administracji PŁ odpowiedzialna za wszelkie finanse na PŁ</dd>
            <dt>Odwoławcza Komisja Stypendialna (OKS)</dt>
              <dd>organ drugiej instancji, do którego odwołujemy się jeżeli nie zgadzamy się z decyzją wydaną przez Komisję Stypendialną.</dd>
            <dt>Okres zasiłkowy</dt>
              <dd>czas, na który staramy się o stypendium socjalne (domyślnie od miesiąca złożenia podania  do czerwca).</dd>
            <dt>Opiekun faktyczny</dt>
              <dd>oznacza to osobę opiekującą się dzieckiem, która wystąpiła z wnioskiem do sądu rodzinnego o przysposobienie dziecka - przyznanie praw rodzicielskich. Do czasu pomyślnego wyroku sądu nie można takiej osoby nazywać opiekunem prawnym, dlatego stworzono pojęcie opiekuna faktycznego.</dd>
            <dt>Opiekun prawny</dt>
              <dd>oznacza to osobę, która poprzez zasądzenie sądowe ma przyznane prawa rodzicielskie nad dzieckiem. Zwykle jest to rodzic biologiczny, lecz w niektórych przypadkach sąd decyduje o ustaleniu innego opiekuna np. dziadka lub babci.</dd>
            <dt>Osoba fizyczna</dt>
              <dd>prawne określenie każdego człowieka, od chwili urodzenia do chwili śmierci.</dd>
            <dt>PSW</dt>
              <dd>ustawa prawo o szkolnictwie wyższym.</dd>
            <dt>Rok bazowy</dt>
              <dd>rok, którego dotyczą zaświadczenia z Urzędu Skarbowego.</dd>
            <dt>Samodzielność finansowa</dt>
              <dd>w określonych sytuacjach student przy wykazywaniu członków gospodarstwa domowego może pominąć swoich rodziców/opiekunów i dzieci będących na ich utrzymaniu. Jest to m.in. posiadanie własnego potomstwa lub bycie w związku małżeńskim, wszystkie przypadki podane są w ust.22 Rozdział VII RPM.</dd>
            <dt>System SID</dt>
              <dd>system obsługujący dane dotyczące studentów na naszej uczelni.</dd>
            <dt>Urlop dziekański</dt>
              <dd>jest to przerwa w studiowaniu trwająca od jednego do dwóch semestrów, na którą student został skierowany przez dziekana np. w przypadku niezaliczenia semestru (czyli dziekan wysyła studenta na urlop).</dd>
            <dt>Urlop okolicznościowy</dt>
              <dd>jest to przerwa w studiowaniu trwająca od jednego do dwóch semestrów, na którą student został skierowany po złożeniu odpowiedniego wniosku (czyli student sam wnioskuje do dziekana o urlop).</dd>
            <dt>Załącznik A</dt>
              <dd>są to zaświadczenia z Urzędu Skarbowego o dochodzie opodatkowanym za rok poprzedzający rok akademicki, w którym składamy podanie o stypendium socjalne.</dd>
            <dt>Załącznik C</dt>
              <dd>jest to wypełniane samodzielnie przez każdego członka rodziny oświadczenie dotyczące dochodów niepodlegających opodatkowaniu. Pełna lista źródeł dochodów, które należy wpisać, znajduje się w pouczeniu Załącznika C.</dd>
            <dt>Załączniki D</dt>
              <dd>zaświadczenia wypełniane przez pracodawcę, mówiące o okresie zatrudnienia za ten sam rok, którego dotyczą zaświadczenia z Urzędu Skarbowego.</dd>
            <dt>Załączniki D1</dt>
              <dd>zaświadczenia wypełniane przez pracodawcę, mówiące o okresie zatrudnienia jeśli praca została podjęta dopiero po roku, za który przynosimy zaświadczenia z Urzędu Skarbowego.</dd>
          </dl>
        </div>
        <button @click="showDict = false" class="modal-close is-large" aria-label="close"></button>
      </div>

    <div class="has-text-centered">
      <button  @click="showDict = true" class="button">Zobacz słowniczek</button>
    <footer class="columns">
      <div class="column">
        <img :src="prefix + 'logo.png'" alt="">
      </div>
      <div class="column">
        <p>
          <strong>Komisja ds. Pomocy Materialnej</strong><br>
          <a href="mailto:pomoc.materialna@samorzad.p.lodz.pl">pomoc.materialna@samorzad.p.lodz.pl</a><br>
          Dyżury: <a href="https://samorzad.p.lodz.pl/">samorzad.p.lodz.pl</a><br>
          al. Politechniki 3a (I piętro), 90-924 Łódź
        </p>
      </div>
      <div class="column">
        <img :src="prefix + 'logo-pl.png'" alt="">
      </div>
    </footer>
    <p>Skrypt został stworzony przez <a href="https://samorzad.p.lodz.pl/" target="_blank">Samorząd Studencki Politechniki Łódzkiej</a>. Wszystkie prawa zastrzeżone.</p>
    <small>Wersja {{ version }}</small>
  </div>
    </div>
      </div>

  `,
};