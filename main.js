 let store = {
    financiallyIndependent: false,
    notFromLodz: false,
    noIncomeFamily: true,

    step: 'setup',

    family: [],

    documentLists: [],

    otherDocuments: []
  } 

  let lastYear = 2021;

  function DocumentDetails(title, requiredType) {
    this.title = title
    this.requiredType = requiredType
  }

  let documents = {
    'motherDeathCert': new DocumentDetails('Akt zgonu matki', 'Kopia'),
    'fatherDeathCert': new DocumentDetails('Akt zgonu ojca', 'Kopia'),
    'divorceDecision': new DocumentDetails('Odpis prawomocnego orzeczenia sądu orzekającego rozwód lub separację', 'Kopia'),
    'motherMissingProof': new DocumentDetails('Zaświadczenie o zgłoszeniu zaginięcia matki', 'Oryginał'),
    'fatherMissingProof': new DocumentDetails('Zaświadczenie o zgłoszeniu zaginięcia ojca', 'Oryginał'),
    'motherLockedFacilityProof': new DocumentDetails('Dokument potwierdzający przebywanie matki w ośrodku zamkniętym', 'Oryginał'),
    'fatherLockedFacilityProof': new DocumentDetails('Dokument potwierdzający przebywanie ojca w ośrodku zamkniętym', 'Oryginał'),
    'fullBirthCert': new DocumentDetails('Odpis zupełny aktu urodzenia wnioskodawcy', 'Oryginał odpisu'),

    'schoolProof': new DocumentDetails('Zaświadczenie ze szkoły o pobieraniu nauki lub skrócony akt urodzenia (jeśli ojciec jest nieznany, to pełny akt urodzenia)', 'Oryginał zaświadczenia lub oryginał odpisu aktu'),
    'studyProof': new DocumentDetails('Zaświadczenie ze szkoły/uczelni o pobieraniu nauki', 'Oryginał'),
    'disabilityProof': new DocumentDetails('Orzeczenie o niepełnosprawności lub stopniu niepełnosprawności', 'Oryginał lub kopia poświadczona za zgodność z oryginałem - oryginał do wglądu'),
    'shortBirthCert': new DocumentDetails('Skrócony akt urodzenia wnioskodawcy', 'Oryginał odpisu'),
    'shortBirthCertSibling': new DocumentDetails('Skrócony akt urodzenia (jeśli ojciec jest nieznany, to pełny akt urodzenia)', 'Oryginał odpisu'),

    // Praca
    'attachmentD': new DocumentDetails('Zaświadczenie z miejsca pracy określające, przez ile miesięcy w roku ' + lastYear + ' wypłacane było wynagrodzenie', 'Wypełniony wzór D do załącznika nr 4'),
    'attachmentDwithZUS': new DocumentDetails('Zaświadczenia z miejsca pracy określające, przez ile miesięcy w roku ' + lastYear + ' wypłacane było wynagrodzenie oraz zaświadczenie z ZUS-u/KRUS-u o wysokości odprowadzonych składek zdrowotnych (jeżeli nie rozliczasz składek w ZUS-ie/ KRUS-ie – nie przynoś zaświadczenia)', 'Wypełniony wzór'),
    // Działalność gospodarcza
    'businessProof': new DocumentDetails('Dla osób prowadzących pozarolniczą działalność gospodarczą opodatkowaną w formie ryczałtu ewidencjonowanego lub karty podatkowej – zaświadczenie naczelnika urzędu skarbowego, dotyczące członków rodziny rozliczających się na podstawie przepisów o zryczałtowanym podatku dochodowym od niektórych przychodów osiąganych przez osoby fizyczne w roku kalendarzowym ' + lastYear + '; W przypadku rozliczania na zasadach ogólnych: wzór D do załącznika nr 4 wypełniony przez osobę prowadzącą działalność lub księgową', 'Oryginały'),
    'businessStartDateProof': new DocumentDetails('wypis z Centralnej Ewidencji i Informacji o Działalności Gospodarczej (CEIDG) zawierający datę rozpoczęcia działalności gospodarczej', 'Oryginał'),
    // Gospodarstwo
    'fieldSizeProof': new DocumentDetails('•  nakaz płatniczy oraz zaświadczenie właściwego organu gminy o wielkości gospodarstwa rolnego wyrażonego w hektarach ogólnych oraz przeliczeniowych. Zaświadczenie winno określać stan posiadania w roku ' + lastYear, 'Oryginał lub kopia poświadczona za zgodność z oryginałem - oryginał do wglądu'),
    'paymentOrder': new DocumentDetails('Nakaz płatniczy (wystawiany przez gminę na posiadacza gospodarstwa rolnego)', 'Oryginał lub kopia poświadczona za zgodność z oryginałem - oryginał do wglądu'),
    'KRUSProof': new DocumentDetails('Zaświadczenie z KRUS-u o składkach na ubezpieczenie zdrowotne', 'Oryginał'),
    'leaseAgreement': new DocumentDetails('Umowa dzierżawy – w przypadku oddania w dzierżawę części lub całości znajdującego się w posiadaniu studenta/doktoranta lub jego rodziny gospodarstwa rolnego (zawartą zgodnie z przepisami o ubezpieczeniu społecznym rolników tj. gdy wydzierżawiającym jest emeryt lub rencista rolniczy oraz umowa dzierżawy zawarta została na min.10 lat, której zawarcie potwierdził wójt, właściwy ze względu na miejsce położenia przedmiotu dzierżawy, a także dzierżawcą nie jest małżonek, zstępny, pasierb…), w przypadku wniesienia gospodarstwa rolnego do użytkowania przez rolniczą spółdzielnię produkcyjną: umowa zawartą w formie aktu notarialnego', 'Oryginał lub kopia poświadczona za zgodność z oryginałem - oryginał do wglądu'),
    // Renta
    'pensionProof': new DocumentDetails('Decyzje określające prawo do renty uwzględniające min. cały rok ' + lastYear + ' do daty końcowej decyzji oraz decyzje o przyznaniu dodatkowych świadczeń pieniężnych w roku ' + lastYear + ' (tzw. trzynasta i czternasta renta)', 'Oryginał'),
    // Emerytura 
    'retirementProof': new DocumentDetails('Decyzje określające prawo do emerytury uwzględniające min. cały rok ' + lastYear + ' do daty końcowej decyzji oraz decyzje o przyznaniu dodatkowych świadczeń pieniężnych w roku ' + lastYear + ' (tzw. trzynasta i czternasta emerytura)', 'Oryginał'),
    // Alimenty
    'alimonyProof': new DocumentDetails('Odpis podlegającego wykonaniu orzeczenia sądowego zasądzającego alimenty na rzecz osób w rodzinie lub poza rodziną, lub odpis protokołu posiedzenia zawierającego treść ugody sądowej, lub odpis zatwierdzonej przez sąd ugody zawartej przed mediatorem, lub innego tytułu wykonawczego pochodzącego lub zatwierdzonego przez sąd (np. zabezpieczenie na poczet powództwa cywilnego w przypadku gdy postepowanie jest w toku), zobowiązujących do alimentów na rzecz osób w rodzinie lub poza rodziną; W przypadku gdy osoba uprawniona nie otrzymała alimentów albo otrzymała je w wysokości niższej od ustalonej w wyroku sądu, ugodzie sądowej lub ugodzie zawartej przed mediatorem lub innym tytule wykonawczym pochodzącym lub zatwierdzonym przez sąd: zaświadczenie organu prowadzącego postępowanie egzekucyjne o całkowitej lub częściowej bezskuteczności egzekucji alimentów, a także o wysokości wyegzekwowanych alimentów LUB informację właściwego sądu lub właściwej instytucji o podjęciu przez osobę uprawnioną czynności związanych z wykonaniem tytułu wykonawczego za granicą albo o niepodjęciu tych czynności, w szczególności w związku z brakiem podstawy prawnej do ich podjęcia lub brakiem możliwości wskazania przez osobę uprawnioną miejsca zamieszkania dłużnika alimentacyjnego za granicą, jeżeli dłużnik zamieszkuje za granicą; zaświadczenie o pobieraniu/niepobieraniu świadczeń z Funduszu Alimentacyjnego; odpis prawomocnego wyroku oddalającego powództwo o ustalenie alimentów; orzeczenie sądu zobowiązujące jednego z rodziców do ponoszenia całkowitych kosztów utrzymania dziecka; przekazy lub przelewy pieniężne dokumentujące wysokość zapłaconych alimentów, jeżeli członkowie rodziny są zobowiązani wyrokiem sądu, ugodą sądową lub ugodą zawartą przed mediatorem do ich płacenia na rzecz osoby spoza rodziny; UWAGA: Przy ocenie sytuacji materialnej studenta bierze się pod uwagę obowiązek alimentacyjny rodziców wobec dzieci – art. 133 kodeksu rodzinnego i opiekuńczego. Fakt samodzielnego zamieszkiwania studenta poza domem rodzinnym bez spełniania wymogów zawartych w ust. 21 rozdz. 7 Regulaminu nie zwalnia jego rodziców od obowiązku alimentacyjnego w okresie studiów.', 'Kopia (oryginały w przypadku zaświadczeń)'),
    // Zasiłek chorobowy
    'sicknessBenefitProof': new DocumentDetails('Zaświadczenie z ZUS-u/KRUS-u o wysokości i okresie pobierania zasiłku chorobowego', 'Oryginał'),
    // Dieta
    'perDiemProof': new DocumentDetails('Dokument potwierdzający otrzymywanie diety', 'Oryginał'),
    // Bezrobocie
    'unemploymentProof': new DocumentDetails('Zaświadczenie z urzędu pracy lub oświadczenie potwierdzające fakt pozostawania bez pracy z prawem lub bez prawa do zasiłku', 'Oryginał'),
    // Zagraniczne
    'workedAbroad': new DocumentDetails('Zaświadczenie o wysokości osiągniętego dochodu w roku ' + lastYear, 'Oryginał'),
    'other': new DocumentDetails('Jeżeli ta osoba posiada dochody inne niż zostały uwzględnione i nie wiesz, jak je udokumentować, skonsultuj się z Sekcją Obsługi Świadczeń lub Komisją ds. Pomocy Materialnej i upewnij się co do sposobu ich udokumentowania. Dla dochodu uzyskanego w roku ' + lastYear + ' dostarcz dokument określający wysokość dochodu uzyskanego oraz liczbę miesięcy, w których dochód był osiągany. Dla dochodu uzyskanego w roku ' + lastYear+1 + ' lub ' + lastYear+1 + 'dokument określający datę uzyskania w zależności od sytuacji: zaświadczenie z zakładu pracy o zatrudnieniu (Wzór D1 do załącznika nr 4), zaświadczenie z Urzędy Pracy o przyznaniu zasiłku dla bezrobotnych oraz wysokość dochodu netto otrzymanego za miesiąc następujący po miesiącu, w którym dochód został uzyskany', 'Forma zależna od wymaganego dokumentu'),

    'USProof': new DocumentDetails('Zaświadczenia z Urzędu Skarbowego dokumentujące dochody podlegające opodatkowaniu na zasadach określonych w art. 26, art. 30b, art. 30c, art. 30e i art. 30f ustawy o podatku dochodowym od osób fizycznych (nawet jeżeli osoba nie osiąga dochodów) za rok ' + lastYear + ' – wydane na użytek świadczeń rodzinnych, z uwzględnieniem przychodów wolnych od podatku (w tym PIT dla młodych)', 'Wypełniony wzór A do załącznika nr 4'),
    'attachmentC': new DocumentDetails('Oświadczenie o dochodach nieopodatkowanych podatkiem dochodowym od osób fizycznych za rok ' + lastYear + ' (nawet jeżeli w tym roku nie osiągali dochodów)', 'Wypełniony wzór C do załącznika nr 4'),
    'ZUSProof': new DocumentDetails('Zaświadczenie o wysokości faktycznie zapłaconych składek na ubezpieczenie zdrowotne w roku ' + lastYear + ' (zaświadczenia odpowiednio z ZUS, KRUS, MSW lub MON – w przypadku składek odprowadzanych z emerytury lub renty: z podziałem na miesiące)', 'Oryginał'),
    'newIncomeInfo': new DocumentDetails('Jeżeli ta osoba posiada dochody, których nie uzyskiwała w roku ' + lastYear + ' i nie wiesz, jak je udokumentować, skonsultuj się z Sekcją Obsługi Świadczeń lub Komisją ds. Pomocy Materialnej i upewnij się co do sposobu ich udokumentowania. (W przypadku rozpoczęcia pracy powinieneś dostarczyć załącznik D1 wypełniony przez pracodawcę)', ''),
    'incomeTaxProof': new DocumentDetails('Zaświadczenie z Urzędu Skarbowego o wysokości dochodu w podatku dochodowym od osób fizycznych za rok ' + lastYear, 'Oryginał'),
    'unemploymentNotRegistered': new DocumentDetails('Oświadczenie o braku rejestracji w PUP i niepobieraniu świadczeń z tego tytułu', 'Własne oświadczenie'),
    'noIncomeDeclaration': new DocumentDetails('Oświadczenie o niepracowaniu i nieuzyskiwaniu żadnych innych dochodów', 'Własne oświadczenie'),

    'employmentCert': new DocumentDetails('Świadectwo pracy lub umowa o pracę + PIT11 z utraconej pracy', 'Kopia'),
    'endOfBusinessProof': new DocumentDetails('Dokument potwierdzający zakończenie lub zawieszenie prowadzenia działalności gospodarczej', 'Kopia decyzji'),
    'fieldSoldProof': new DocumentDetails('Dokument poświadczający sprzedaż ziemi', 'Oryginał lub kopia poświadczona za zgodność z oryginałem (oryginał do wglądu)'),
    'lostPensionProof': new DocumentDetails('Zaświadczenie o utracie prawa do otrzymywania renty ', 'Oryginał'),
    'lostRetirementProof': new DocumentDetails('Zaświadczenie o zmianie wysokości i okresie pobierania emerytury', 'Oryginał'),
    'lostAlimonyProof': new DocumentDetails('Zaświadczenie o bezskuteczności egzekucji alimentów', 'Kopia'),
    'lostSicknessBenefitProof': new DocumentDetails('Zaświadczenie z ZUS-u/KRUS-u o wysokości i okresie pobierania zasiłku chorobowego', ''),
    'lostUnemploymentProof': new DocumentDetails('PIT-11 dotyczący zasiłku dla bezrobotnych', 'Kopia'),
    'PIT11or40': new DocumentDetails('Dokument określający datę utraty dochodu oraz wysokość utraconego dochodu: świadectwo pracy, umowę zlecenie, PIT-11/PIT-40 dokumentujący wysokość utraconego dochodu' + lastYear, 'Wypełniony wzór C do załącznika nr 4'),

    'marriageAct': new DocumentDetails('Akt małżeństwa', 'Kopia'),
    'childBirthCert': new DocumentDetails('Akt urodzenia dziecka', 'Oryginał odpisu'),

    'notCounted': new DocumentDetails('Ta osoba nie jest wliczana do gospodarstwa domowego', ''),

    'accomodationBenefit': new DocumentDetails('Podanie o zwiększenie z tytułu zamieszkania w Domu Studenckim lub innym obiekcie', ''),
    'OPSProof': new DocumentDetails('Zaświadczenie o sytuacji dochodowej i majątkowej swojej i rodziny z Ośrodka Pomocy Społecznej', ''),
  }

  let lostIncomeJoin = {
    'attachmentDwithZUS': 'employmentCert',
    'attachmentD': 'employmentCert',
    'businessProof': 'endOfBusinessProof',
    'pensionProof': 'lostPensionProof',
    'retirementProof': 'lostRetirementProof',
    'sicknessBenefitProof': 'lostSicknessBenefitProof',
    'unemploymentProof': 'lostUnemploymentProof',
  } 

  let lostIncomeLabels = {
    'employmentCert': 'Dochód z pracy',
    'endOfBusinessProof': 'Dochód z prowadzenia działalności gospodarczej',
    'lostPensionProof': 'Renta',
    'lostRetirementProof': 'Emerytura',
    'lostSicknessBenefitProof': 'Zasiłek chorobowy',
    'lostUnemploymentProof': 'Zasiłek dla bezrobotnych',
  } 

  let familyMembersJoin = {
    'student': 'Wnioskodawca',
    'father': 'Ojciec',
    'mother': 'Matka',
    'guardian': 'Opiekun prawny',
    'sibling': 'Brat/siostra',
    'spouse': 'Małżonek',
    'child': 'Dziecko',
  }


  let vm = new Vue({
    el: '#app',
    data: {
      window: window,
      storage: store,
      showDict: false,
      editMode: false,
      lastYear: lastYear,
      currentId: 0,
      isLearning: true,
      isDisabled: false,
      hasNewIncome: false,
      siblingProcessed: false,
      documentsToAdd: [],
      newFamilyMember: {
        id: 0,
        name: "",
        relation: "",
        age: "",
      },
    },

    computed: {
      anyFamilyMembers() {
        return (this.storage.family.length > 0 && !!this.storage.family.find(element => element.relation == 'student'));
      },
      currentFamilyMember() {
        return (this.storage.family[this.currentId]);
      },
      currentFamilyMemberNo() {
        return this.currentId+1;
      },
      countFamilyMembers() {
        return this.storage.family.length;
      },
      enteredMissingParentDetails() {
        return this.documentsToAdd.length > 0;
      },
      filledNewFamilyMemberFields() {
        return (!!this.newFamilyMember.name && !!this.newFamilyMember.relation && !!this.newFamilyMember.age);
      }, 
      getCurrentAge() {
        return parseInt(this.storage.family[this.currentId].age, 10);
      },
      hasMissingParents() {
        return (!this.storage.financiallyIndependent && (!this.hasMember('mother') || !this.hasMember('father')) && !this.hasMember('guardian'));
      },
      hasOtherDocuments() {
        return this.storage.otherDocuments.length > 0;
      },
      incomeDocuments() {
        return this.documentsToAdd.filter(doc => this.isIncomeDocument(doc));
      },
      lostIncomeDocuments() {
        return this.documentsToAdd.filter(doc => this.isLostIncomeDocument(doc));
      },
      isAdultSibling () {
        if (this.storage.family[this.currentId]) {
          return (this.storage.family[this.currentId].relation == 'sibling' && this.getCurrentAge > 18);
        } else {
          return false;
        }
      },
      isChild () {
        if (this.storage.family[this.currentId]) {
          return (this.storage.family[this.currentId].relation == 'child');
        } else {
          return false;
        }
      },
      isFinanciallyIndependent() {
        return this.storage.financiallyIndependent;
      },
      isParentOrGuardian () {
        if (this.storage.family[this.currentId]) {
          return (this.storage.family[this.currentId].relation == 'mother' || this.storage.family[this.currentId].relation == 'father' || this.storage.family[this.currentId].relation == 'guardian');
        } else {
          return false;
        }
      },
      isSibling () {
        if (this.storage.family[this.currentId]) {
          return (this.storage.family[this.currentId].relation == 'sibling');
        } else {
          return false;
        }
      },
      isStudent () {
        if (this.storage.family[this.currentId]) {
          return (this.storage.family[this.currentId].relation == 'student');
        } else {
          return false;
        }
      },
      hasLostIncome() {
        return this.documentsToAdd.filter((v) => { return Object.keys(lostIncomeLabels).includes(v) }).length > 0;
      },
      hasField() {
        return this.documentsToAdd.indexOf('fieldSizeProof') >= 0;
      },
      onlyStudentUnder26() {
        return (this.storage.family.length === 1 && !!this.storage.family.find(element => element.relation == 'student') && (parseInt(this.storage.family[0].age, 10) < 26));
      },
    },

    methods: {
      findDocument(doc) {
        return documents[doc];
      },
      findDocumentNameWithType(doc) {
        foundDoc = this.findDocument(doc)
        return foundDoc.title + ((foundDoc.requiredType == '') ? '' : ' — ' + foundDoc.requiredType);
      },
      findLostIncomeDocument(doc) {
        return lostIncomeJoin[doc];
      },
      findFamilyMemberRelation(member) {
        return familyMembersJoin[member];
      },
      findLostIncomeDocumentLabel(doc) {
        return lostIncomeLabels[doc];
      },
      isIncomeDocument(doc) {
        return Object.keys(lostIncomeJoin).indexOf(doc) >= 0;
      },
      isLostIncomeDocument(doc) {
        return Object.keys(lostIncomeLabels).indexOf(doc) >= 0;
      },
      isMemberCounted(index) {
        return  this.storage.documentLists[index].indexOf('notCounted') < 0;
      },
      goToStep(stepName) {
        if (stepName == 'processMembers' && this.isSibling && !this.siblingProcessed) {
          this.processSibling();
        } else if (stepName == 'processMembers' && this.isChild) {
          this.nextMember();
        } else if (stepName == 'processLostIncome' && ((this.documentsToAdd.filter(doc => this.isIncomeDocument(doc)).length < 1) || (this.documentsToAdd.indexOf('other') >= 0 && this.documentsToAdd.filter(doc => this.isIncomeDocument(doc)).length == 0))) {
          this.storage.step = 'processNewIncome';
        } else {
          this.storage.step = stepName;
        }
      },
      addMissingParentDetails(relation) {
        this.storage.otherDocuments.push(this.documentsToAdd);
        this.documentsToAdd = [];
        this.goToStep((relation == 'mother' && !this.hasMember('father')) ? 'missingFather' : 'processMembers');
      },
      addDocuments() {
        this.storage.documentLists[this.currentId] = this.documentsToAdd.splice(0);
        this.documentsToAdd = [];
      },
      hasMember(relation) {
        return (this.storage.family.find(element => element.relation == relation));
      },
      finishFamilySetup() {
        if ( this.hasMissingParents ) {
          this.goToStep((!this.hasMember('mother')) ? 'missingMother' : 'missingFather');
        // } else if (this.isFinanciallyIndependent && this.storage.family.length == 1 && this.storage.family[0].age < 26) {
        //   this.goToStep('notEligibleForIndependency');
        } else {
          this.goToStep('processMembers');
        };
      },
      findFamilyMember(id) {
        return this.storage.family.find(element => element.id == id);
      },
      addFamilyMember() {
        this.storage.family.push(Vue.util.extend({}, this.newFamilyMember));
        this.newFamilyMember = {
          id: this.newFamilyMember.id + 1,
          name: '',
          age: '',
          relation: '',
        };
      },
      removeFamilyMember(memberId) {
        var removedIndex = this.storage.family.indexOf(this.storage.family.find(element => element.id == memberId));
        this.storage.family.splice(removedIndex, 1);
      },
      editFamilyMember(memberId) {
        this.editMode = true;
        var indexToEdit = this.storage.family.indexOf(this.storage.family.find(element => element.id == memberId));
        this.newFamilyMember = Vue.util.extend({}, this.storage.family[indexToEdit]);
      },
      saveFamilyMember() {
        this.editMode = false;
        var indexToEdit = this.storage.family.indexOf(this.storage.family.find(element => element.id == this.newFamilyMember.id));
        this.storage.family[indexToEdit] = Vue.util.extend({}, this.newFamilyMember);
        this.newFamilyMember = {
          id: Math.max.apply(Math,this.storage.family.map(function(o){return o.id;})) + 1,
          name: '',
          age: '',
          relation: '',
        };
      },
      processSibling() {
        if (this.isAdultSibling && !this.siblingProcessed) {
          this.storage.step = 'processMembersSibling';
        } else {
          this.goToMemberIncome();
        } 
      },
      goToMemberIncome() {
        this.siblingProcessed = true;
        if (this.getCurrentAge <= 18) {
          this.getCurrentAge <= 6 ? this.documentsToAdd.push('shortBirthCertSibling') : this.documentsToAdd.push('schoolProof');
          this.nextMember();
        } else {
          this.documentsToAdd.push('attachmentC');
        }

        if (this.siblingProcessed) {
          if (this.isLearning && this.getCurrentAge <= 26) {
            this.documentsToAdd.push('studyProof');
            this.goToStep('processMembers');
          } 
  
          if (this.isDisabled) {
            this.documentsToAdd.push('disabilityProof');
            this.goToStep('processMembers');
          }

          else if (this.getCurrentAge >= 27 || (this.getCurrentAge < 27 && !this.isLearning)) {
            this.documentsToAdd.push('notCounted');
            this.nextMember();
          }
        }

      },
      processNoIncome() {
        if (this.isParentOrGuardian) {
          if (this.documentsToAdd.length == 0 || 
              (this.incomeDocuments.length == this.lostIncomeDocuments.length) ||
              (this.documentsToAdd.length == 1 && this.documentsToAdd.indexOf('unemploymentNotRegistered') > -1)) {
                this.documentsToAdd.push('noIncomeDeclaration');
          }
          else {
            this.storage.noIncomeFamily = false;
          }
        }
      },
      processZUSProof() {
        if ((this.isParentOrGuardian || this.isAdultSibling) && this.documentsToAdd.indexOf('KRUSProof') < 0 && this.documentsToAdd.indexOf('noIncomeDeclaration') < 0) {
            this.documentsToAdd.unshift('ZUSProof');
        }
      },

      processBusinessProof() {
        if (this.documentsToAdd.indexOf('businessProof') > -1) {
          this.documentsToAdd.unshift('businessStartDateProof');
        }
      },
      processField() {
        if (this.isParentOrGuardian && this.documentsToAdd.indexOf('fieldSizeProof') > -1) {
            this.documentsToAdd.push('paymentOrder');
        }        
      },
      clearLostIncomeDocs() {
        index = this.documentsToAdd.indexOf('attachmentDwithZUS') > -1 ? this.documentsToAdd.indexOf('attachmentDwithZUS') : this.documentsToAdd.indexOf('attachmentD');

        if (index > -1 && this.documentsToAdd.indexOf('employmentCert') > -1) {
          this.documentsToAdd.splice(index, 1);
        }

      },
      nextMember() {
        this.processNoIncome();
        this.processZUSProof();
        this.processBusinessProof();
        this.processField();
        this.clearLostIncomeDocs();

        if (this.storage.family[this.currentId].age > 18) this.documentsToAdd.unshift('USProof', 'attachmentC');
        if (this.storage.family[this.currentId].relation == 'sibling' && this.storage.family[this.currentId].age > 18 && this.storage.family[this.currentId].age < 26)  this.documentsToAdd.push('incomeTaxProof');
        if (this.storage.family[this.currentId].relation == 'sibling' && this.storage.family[this.currentId].age > 26 && this.isDisabled) this.documentsToAdd.push('incomeTaxProof');
        if (this.storage.family[this.currentId].relation == 'spouse') this.documentsToAdd.push('marriageAct'); 
        if (this.storage.family[this.currentId].relation == 'child') this.documentsToAdd.push('childBirthCert');
        
        if (this.hasNewIncome) this.documentsToAdd.push('newIncomeInfo');
        if (this.hasLostIncome) this.documentsToAdd.push('PIT11or40');

        this.addDocuments();

        this.currentId++;
        this.hasNewIncome = false;
        this.siblingProcessed = false;
        if (this.currentId >= this.storage.family.length) {
          this.goToStep('additionalInfo');
        } else {
          this.goToStep('processMembers');
        }
      },
      reset() {
        this.storage.family = [];
        this.newFamilyMember = {
          id: 0,
          name: '',
          age: '',
          relation: '',
        };

        this.goToStep('setup');
      },
      endAlgorithm() {
        if (this.storage.notFromLodz) this.storage.otherDocuments.push('accomodationBenefit');
        if (this.storage.noIncomeFamily) this.storage.otherDocuments.push('OPSProof');

        this.goToStep('finish');
      },
      addTextToPDF(text, settings) {
        let splitTitle = settings.doc.splitTextToSize(text, 270);
        let pageHeight = settings.doc.internal.pageSize.height;
       
        for (let i = 0; i < splitTitle.length; i++) {                
          if (settings.y > 280) {
              settings.y = 10;
              doc.addPage();
          }
          settings.doc.text(15, settings.y, splitTitle[i]);
          settings.y = settings.y + 7;
        }
      },
      /*PDFExport() {
        var doc = new jsPDF();
        doc.setFontType("normal");
        doc.setFontSize("11");

        var y = 7;

        let settings = {
          doc: doc,
          y: y
        }

        this.addTextToPDF('Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!Hello world!', settings);
        doc.save('ListaDokumentow.pdf');
      }, */
    },
  });